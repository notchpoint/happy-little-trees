import * as React from 'react';

export default function Header() {
  return (
    <header>
      <nav>
        <ol>
          <li>
            <a href="/">Home</a>
          </li>
        </ol>
      </nav>
    </header>
  );
}
