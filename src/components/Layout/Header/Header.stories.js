import React from 'react';

import Header from './Header';

export default {
  title: 'Header',
  component: Header,
};

const Template = args => <Header {...args} />;

export const FirstStory = Template.bind({});

// FirstStory.args = {
//   /*👇 The args you need here will depend on your component */
// };
