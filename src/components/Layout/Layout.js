import * as React from 'react';

import Header from './Header';
import './Layout.css';

export default function Layout({ children }) {
  return (
    <React.Fragment>
      <Header />
      <main>{children}</main>
    </React.Fragment>
  );
}
