import faker from 'faker';
import * as React from 'react';

import Layout from 'components/Layout';

function HomePage() {
  return (
    <Layout>
      <h1>Happy Little Trees</h1>
      <p>{faker.lorem.paragraph()}</p>
      <p>{faker.lorem.paragraph()}</p>
    </Layout>
  );
}

export default HomePage;
