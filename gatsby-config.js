module.exports = {
  siteMetadata: {
    title: 'Happy Little Trees',
  },
  plugins: ['gatsby-plugin-postcss', 'gatsby-plugin-resolve-src'],
};
