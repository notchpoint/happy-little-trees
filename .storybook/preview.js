import renderToHTML from './renderToHTML';

// const STORY_SORT_LIST = [];

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  docs: {
    transformSource: (_, storyContext) => renderToHTML(storyContext.storyFn),
  },
  // options: {
  //   storySort: {
  //     method: '',
  //     order: STORY_SORT_LIST,
  //     locales: '',
  //   },
  // },
  // previewTabs: { 'storybook/docs/panel': { index: -1 } },
  // viewMode: 'docs',
};
